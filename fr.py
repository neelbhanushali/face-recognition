import os
import random
import json
import face_recognition

training_img_folder = 'training_images'
test_img_folder = 'test_images'
model = 'model.json'

def generate_dataset():
    dataset = []
    for name in os.listdir(training_img_folder):
        for img in os.listdir(f"{training_img_folder}/{name}"):
            dataset.append({'identifier': name, 'image': f"{training_img_folder}/{name}/{img}"})

    return dataset


def train_data(data, model):
    # Loading data from model
    training_data = None
    if os.path.exists(model):
        with open(model, 'r') as f:
            training_data = f.read()
        f.close()
    training_data = json.loads(training_data or '[]')

    print('Training...')
    for i in data:
        print(i['image'])
        img = face_recognition.load_image_file(i['image'])
        encoding = face_recognition.face_encodings(img)
        if encoding:
            training_data.append({'identifier': i['identifier'], 'encoding': encoding[0].tolist()})
    
    # updating model
    with open(model, 'w') as f:
        f.write(json.dumps(training_data))
    f.close()
    print('Training done...')


def generate_test_data():
    dataset = []
    for img in os.listdir(test_img_folder):
        dataset.append(f"{test_img_folder}/{img}")

    return dataset

def identify(image, model):
    training_data = None
    if os.path.exists(model):
        with open(model, 'r') as f:
            training_data = f.read()
        f.close()

    training_data = json.loads(training_data or '[]')
    names = [i['identifier'] for i in training_data]
    values = [i['encoding'] for i in training_data]

    img = face_recognition.load_image_file(image)
    encodings = face_recognition.face_encodings(img)
    if encodings:
        encoding = encodings[0]

        result = face_recognition.compare_faces(values, encoding)
        names_ = []
        for i, res in enumerate(result):
            if res:
                names_.append(names[i])
        names__ = set(names_)
        names_dict = {}
        for i in names__:
            names_dict[i] = names_.count(i)
        print(names_dict, '\t', image)
        print()


# data = generate_dataset()
# random.shuffle(data)
# train_data(data, model)

test_data = generate_test_data()
for i in test_data:
    identify(i, model)