import face_recognition
import os
import json
import numpy as np

f = open('3idiots.json')
data_3idiots = json.load(f)
f.close()

f = open('3idiots-cast.json')
data_3idiots_cast = json.load(f)
f.close()

for i, (key, val) in enumerate(data_3idiots_cast.items()):
  print(key)
  for j, (key1, val1) in enumerate(data_3idiots.items()):
    result = face_recognition.compare_faces(np.array(val1), val)
    print('found' if any(result) else 'not found', key1, result)
  print()