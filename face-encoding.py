import face_recognition
import os
import json

def generate_encodings(folder_name):
  encodings = {}
  for file_ in os.listdir(folder_name):
    file = f"{folder_name}/{file_}"
    img = face_recognition.load_image_file(file)
    encoding = face_recognition.face_encodings(img)
    print(file, len(encoding))
    
    encoding_ = [e.tolist() for e in encoding]
    encodings[file] = encoding_
    
  with open(f"{folder_name}.json", 'w') as f:
    f.write(json.dumps(encodings))
  f.close()

generate_encodings('3idiots')
generate_encodings('3idiots-cast')